#!/usr/bin/env python3.5
# -*- coding: future_fstrings -*-

import subprocess


def vm_create(template, name, ip, color, provides_network=False, netvm=''):
    subprocess_args = f'qvm-create --template {template} --label {color} --property ip={ip}' \
                      f' --property provides_network={provides_network} --property netvm={netvm} {name}'
    try:
        # Since the command is provided as a string instead of a list of arguments, the shell parameter is needed
        # For some reason I couldn't make it work with the param list approach
        subprocess.run(subprocess_args, shell=True)

    except subprocess.SubprocessError as e:
        print(f'Failed to create VM {name}: subprocess error: {e}')
    except OSError as e:
        print(f'Failed to create VM {name}: OSError: {e}')


def vm_start(name):
    subprocess_args = f'qvm-start {name}'
    try:
        subprocess.run(subprocess_args, shell=True)

    except subprocess.SubprocessError as e:
        print(f'Failed to start VM {name}: subprocess error: {e}')
    except OSError as e:
        print(f'Failed to start VM {name}: OSError: {e}')


def vm_run(name, command):
    subprocess_args = f'qvm-run {name} \'{command}\''
    try:
        subprocess.run(subprocess_args, shell=True)

    except subprocess.SubprocessError as e:
        print(f'Failed to run \'{command}\' in VM {name}: subprocess error: {e}')
    except OSError as e:
        print(f'Failed to run \'{command}\' in VM {name}: OSError: {e}')


def iptables_router_allow_forward_network(network):
    return f'sudo iptables -I FORWARD -s {network} -d {network} -j ACCEPT'


def iptables_client_allow_input_network(network):
    return f'sudo iptables -I INPUT -s {network} -j ACCEPT'


# TODO: make this safe for bash quote escaping of content
def vm_save_to_file(name, content, file):
    command = f'sudo bash -c "echo {content} >> {file}"'
    vm_run(name, command)


def create_network(network_name, ip_range, number_clients, template='fedora-29', color='purple'):
    router_name = network_name + '-router'
    router_ip = '.'.join(ip_range.split('.')[:3] + ['0'])
    router_iptables = iptables_router_allow_forward_network(ip_range)

    vm_create(template, router_name, router_ip, color, True)
    vm_start(router_name)
    vm_run(router_name, router_iptables)
    vm_save_to_file(router_name, router_iptables, '/rw/config/qubes-firewall-user-script')

    for i in range(1, number_clients + 1):
        box_name = network_name + '-box' + str(i)
        box_ip = '.'.join(ip_range.split('.')[:3] + [str(i)])
        box_iptables = iptables_client_allow_input_network(ip_range)

        vm_create(template, box_name, box_ip, color, False, router_name)
        vm_start(box_name)
        vm_run(box_name, box_iptables)
        vm_save_to_file(box_name, box_iptables, '/rw/config/rc.local')


if __name__ == '__main__':
    import fire
    fire.Fire()
