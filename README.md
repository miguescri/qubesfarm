# QubesFarm

A tool to create networks of virtual machines in QubesOS.

Qubes Farm requires admin privileges to work.

Rigth now only part of functionality can be used from a VM different than **dom0**, due
to the limitation of the Admin API, which doesn't allow the execution of code in the
target machine. This translates in not being able to manage the internals of firewall
and vlans in the new VMs; QubesFarm will only be able to create the desired VMs
and destroy them.

## Usage from dom0

**_WARNING:_** remember that running untrusted code in dom0 is the most dangerous thing 
you can do to a QubesOS system. Not only you are trusting me and my code (which I hope is safe, 
but we never know for sure, amirite?), but also you are trusting the code of the required libraries.
Always check what you are about to run.

Due to security concerns, the connectivity of dom0 is strictly limited to the limited
official repos of QubesOS and Fedora, not even allowing to use directly pip to download Python
packages. That is why we need to prepare everything in a different VM.

1. Start a VM (bonus points for using a disposable VM). Following commands are run in that VM.
1. Ensure that python3.5 is installed (that is what dom0 uses currently): `sudo dnf install python35`
1. Clone this repo: `git clone https://gitlab.com/miguescri/qubesfarm`
1. Create a virtual environment: `cd qubesfarm && python3.5 -m venv venv`
1. Modify `venv/bin/activate` so that line 40 is `VIRTUAL_ENV="$PWD/venv"`. This will ensure that we
can properly activate the environment regardless of where the qubesfarm folder lives.
1. Activate the environment and install the requirements: `source venv/bin/activate && pip install 
-r requirements.txt`
1. Zip the folder: `cd .. && zip qubesfarm.zip -r qubesfarm`
1. Go to **dom0** and run: `qvm-run --pass-io name-of-the-VM 'cat 
"/home/user/qubesfarm.zip"' > qubesfarm.zip`
1. Unzip the folder and source the environment: `unzip qubesfarm.zip && cd qubesfarm &&
source venv/bin/activate`
1. You can now use it:  `./QubesFarm.py --help`

## Usage from other VM (limited)

To use the Admin API from a non dom0 machine, you need to do the following:

1. Create a specific VM with name **farmer** (for example).
1. In **dom0** add the following line to `/etc/qubes-rpc/policy/admin.vm.Create.AppVM`:

    `farmer $adminvm allow,target=$adminvm`
1. In **dom0** add the following line to `/etc/qubes-rpc/policy/include/admin-local-rwx`:

    `farmer $tag:created-by-farmer allow,target=$adminvm`

1. In **dom0** add the following line to `/etc/qubes-rpc/policy/admin.vm.List`:

    `farmer $adminvm allow,target=$adminvm`

1. In **dom0** add the following line to `/etc/qubes-rpc/policy/admin.label.List`:

    `farmer $adminvm allow,target=$adminvm`

1. In **dom0** add the following line to `/etc/qubes-rpc/policy/qubes.WaitForSession`:

    `farmer $tag:created-by-farmer allow,target=$adminvm`

1. Clone this repo in **farmer** and execute it.
